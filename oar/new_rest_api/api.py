import json
import time
import os
import re
import ast

from sqlalchemy.orm import Query, Load
from subprocess import PIPE
from flask import Flask, g, abort, url_for, request, Response, make_response
from flask_restx import Api, Resource, fields, reqparse
from werkzeug.middleware.proxy_fix import ProxyFix
from oar.new_rest_api.utils import WSGIProxyFix, PrefixMiddleware, list_paginate
from oar import VERSION
from passlib.apache import HtpasswdFile
from oar.lib import config, db, Job, Queue, Resource as Rsrc
from oar.lib.resource_handling import remove_resource, get_count_busy_resources, set_resource_state
from oar.new_rest_api.query import APIQuery, APIQueryCollection
from oar.lib.submission import JobParameters, Submission, check_reservation
from oar.cli.oardel import oardel
from oar.cli.oarhold import oarhold
from oar.cli.oarresume import oarresume

from oar.new_rest_api.views.admission_rule import register_adrules_routes,  AdmissionRuleDAO
from oar.new_rest_api.views.colmet import register_colmet_routes
from oar.new_rest_api.views.config import register_config_routes
from oar.new_rest_api.views.frontend import register_front_routes
from oar.new_rest_api.views.job import register_job_routes, JobDAO
from oar.new_rest_api.views.media import register_media_routes, MediaDAO
from oar.new_rest_api.views.resource import register_resource_routes, ResourceDAO

import oar.lib.tools as tools


# MEDIA
API_VERSION = "2.0.0.dev0"


def authenticate():
    g.current_user = request.environ.get("USER", None)
    if g.current_user is not None:
        os.environ["OARDO_USER"] = g.current_user
    else:
        if "OARDO_USER" in os.environ:
            del os.environ["OARDO_USER"]


if "OARDIR" not in os.environ:
    os.environ["OARDIR"] = "/usr/local/lib/oar"
OARDODO_CMD = os.environ["OARDIR"] + "/oardodo/oardodo"
if "OARDODO" in config:
    OARDODO_CMD = config["OARDODO"]


def create_app():

    app = Flask(__name__)
    app.wsgi_app = WSGIProxyFix(app.wsgi_app)
    app.wsgi_app = PrefixMiddleware(app.wsgi_app)

    db.query_class = APIQuery
    db.query_collection_class = APIQueryCollection
    app.before_request(authenticate)
    authorizations = {
        'basicAuth': {
            'type': 'basic',
        },
    }

    api = Api(app, version='1.0', title='OAR OpenAPI',
        description='OAR software openAPI', authorizations=authorizations)

    # namespaces
    nsFrontend = api.namespace('', description='Frontend')
    nsJobs = api.namespace('jobs', description='OAR jobs')
    nsResources = api.namespace('resources', description='OAR resources')
    nsMedia = api.namespace('media', description='OAR media')
    nsConfig = api.namespace('config', description='OAR configuration')
    nsAdmission_Rules = api.namespace('admission_rules', description='OAR admission rules')
    nsColmet = api.namespace('colmet', description='OAR colmet')

    # parsing
    parser = api.parser()

    parser.add_argument('state', action='split')
    parser.add_argument('array', type=int)
    parser.add_argument('start_time', type=int)
    parser.add_argument('stop_time', type=int)
    parser.add_argument('user')
    parser.add_argument('ids', action='split')
    parser.add_argument('tail', type=int)
    parser.add_argument('offset', type=int)
    parser.add_argument('limit', type=int)
    parser.add_argument('hostname', type = str)
    parser.add_argument('properties')
    # parser.add_argument('state', type = str)
    parser.add_argument('data')
    parser.add_argument('mode', type=str)
    parser.add_argument('isArray')

    #####################################################################
    ######################    Models    #################################
    #####################################################################

    job = api.model('Job', {
        'id': fields.Integer(readonly=True, description='job identifier'),
        'array_id': fields.Integer(description='array identifier'),
        'array_index': fields.Integer(description='index of the job in the array'),
        'initial_request': fields.String(description='oarsub initial arguments'),
        'name': fields.String(description='name of the cpuset directory used for this job on each nodes'),
        'env': fields.String(description='environment variables to set for the job'),
        'type': fields.String(enum=['INTERACTIVE', 'PASSIVE'], default='PASSIVE', example='INTERACTIVE',
                              description='specify if the user wants to launch a program or get an interactive shell'),
        'info_type': fields.String(description='some informations about oarsub command'),
        'state': fields.String(enum=['Waiting', 'Hold', 'toLaunch', 'toError', 'toAckReservation', 'Launching', 'Running', 'Suspended', 'Resuming', 'Finishing', 'Terminated', 'Error'], example='Waiting', description='job state'),
        'reservation': fields.String(enum=['None', 'toSchedule', 'Scheduled'], default='None', example='toSchedule', description='specify if the job is a reservation and the state of this one'),
        'message': fields.String(description='readable information message for the user'),
        'scheduler_info': fields.String(description=''),
        'user': fields.String(description='user name'),
        'project': fields.String(description='arbitrary name given by the user or an admission rule'),
        'group': fields.String(description='not used'),
        'command': fields.String(description='program to run'),
        'exit_code': fields.Integer(default=0, description='exit code for passive jobs'),
        'queue_name': fields.String(description='queue name'),
        'properties': fields.String(description='properties that assigned nodes must match'),
        'launching_directory': fields.String(description='path of the directory where to launch the user process'),
        'submission_time': fields.Integer(description='date when the job was submitted'),
        'start_time': fields.Integer(description='date when the job was launched'),
        'stop_time': fields.Integer(description='date when the job was stopped'),
        'file_id': fields.Integer(),
        'accounted': fields.String(enum=['YES', 'NO'], default='NO', example='YES', description='specify if the job was considered by the accounting mechanism or not'),
        'notify': fields.String(description='gives the way to notify the user about the job (mail or script )'),
        'assigned_moldable_job': fields.Integer(description='moldable job chosen by the scheduler'),
        'checkpoint': fields.Integer(description='number of seconds before the walltime to send the checkpoint signal to the job'),
        'checkpoint_signal': fields.Integer(description='signal to use when checkpointing the job'),
        'stdout_file': fields.String(description='file name where to redirect program STDOUT'),
        'stderr_file': fields.String(description='file name where to redirect program STDERR'),
        'resubmit_job_id': fields.Integer(description='if a job is resubmitted then the new one store the previous'),
        'suspended': fields.String(enum=['YES', 'NO'], default='NO', example='YES', description='specify if the job was suspended (oarhold)'),
        'cpuset_name': fields.String(description='name of the cpuset directory used for this job on each nodes'),
        'links': fields.List(fields.List(fields.String()))
    })

    jobInit = api.model('JobInit', {
        'resource': fields.String(required=True, description='the resources description as required by oar (example: “/nodes=1/cpu=2”)'),
        'command': fields.String(required=True, description='a command name or a script that is executed when the job starts'),
        'workdir': fields.String(required=False, description='the path of the directory from where the job will be submited'),
        'param_file': fields.String(required=False, description='he content of a parameters file, for the submission of an array job. For example: {“resource”:”/nodes=1, “command”:”sleep”, “param_file”:”60n90n30”}'),
        'arguments': fields.List(fields.String(required=False, description='every long option that may be passed to the oarsub command is known as a key of the input hash. If the option is a toggle (no value), you just have to set it to “1” (for example: ‘use-job-key’ => ‘1’). Some options may be arrays (for example if you want to specify several ‘types’ for a job)'))
    }, mask='{job_name, job_type}')

    resources = api.model('Resources', {
        'id': fields.Integer(),
        'type': fields.String(description='resource type (used for licence resources for example)'),
        'network_address': fields.String(description='the network address given to the resource'),
        'state': fields.String(enum=['Alive', 'Absent', 'Suspected', 'Dead'], description='resource state'),
        # Alive : the resource is ready to accept a job
        # Absent : the oar administrator has decided to pull out the resource. This computer can come back
        # Suspected : OAR system has detected a problem on this resource and so has suspected it (you can look in the event_logs table to know what has happened). This computer can come back (automatically if this is a “finaud” module decision)
        # Dead : The oar administrator considers that the resource will not come back and will be removed from the pool
        'next_state': fields.String(enum=['UnChanged', 'Alive', 'Dead', 'Absent', 'Suspected'], default='UnChanged', description='state for the resource to switch'),
        'finaud_decision': fields.String(enum=['YES', 'NO'], default='NO', description='tell if the actual state results in a “finaud” module decision'),
        'next_finaud_decision': fields.String(enum=['YES', 'NO'], default='NO', description='tell if the next node state results in a “finaud” module decision'),
        'state_num': fields.Integer(description='corresponding state number (useful with the SQL “ORDER” query)'),
        'suspended_jobs': fields.String(enum=['YES', 'NO'], description='specify if there is at least one suspended job on the resource'),
        'scheduler_priority': fields.Integer(description='arbitrary number given by the system to select resources with more intelligence'),
        'cpuset': fields.Integer(description='field used with the JOB_RESOURCE_MANAGER_PROPERTY_DB_FIELD'),
        'besteffort': fields.String(enum=['YES', 'NO'], description='accept or not besteffort jobs'),
        'deploy': fields.String(enum=['YES', 'NO'], description='specify if the resource is deployable'),
        'expiry_date': fields.Integer(description='field used for the desktop computing feature'),
        'desktop_computing': fields.String(enum=['YES', 'NO'], description='tell if it is a desktop computing resource (with an agent)'),
        'last_job_date': fields.Integer(description='store the date when the resource was used for the last time'),
        'available_upto': fields.Integer(description='used with compute mode features to know if an Absent resource can be switch on'),
        'last_available_upto': fields.Integer(),
        'drain': fields.String(),

        'jobs_uri': fields.String(),
        'node_uri': fields.String(),
        'resource_id': fields.Integer(description='resource identifier'),
        'status': fields.String(enum=['ok', 'deleted'], description='ok or deleted'),
        'uri': fields.String(),
        'cluster': fields.Integer(),
        'cpu': fields.Integer(description='global cluster cpu number'),
        'jobs_uri': fields.String(),
        'licence': fields.String(),
        'network_address': fields.String(description=' 	node name (used to connect via SSH)'),
        'test': fields.String(),
        'switch': fields.String(description='name of the switch')
    })

    resources_details = api.model('ResourcesDetails', {
        'core': fields.Integer(),
        'cpu': fields.Integer(),
        'cpuset': fields.Integer(),
        'network_address': fields.String(),
    })

    resources_others = api.model('ResourcesOthers', {
        'links': fields.FormattedString("-href: /resources/details.yaml?limit=5&offset=2", description='links to previous, current and next resources'),
        'items': fields.List(fields.Nested(resources), description='list of resources'),
        'offset': fields.Integer(description='current offset'),
        'total': fields.Integer(description='total of resources'),
    })

    resources_generate = api.model('ResourcesGenerate', {
        'links': fields.FormattedString("-href: /oarapi-priv/resources/generate.yaml"),
        'items': fields.List(fields.Nested(resources_details), description='list of resources'),
        'offset': fields.Integer(description='current offset'),
        'total': fields.Integer(description='total of resources'),
    })

    resources_inputs = api.model('ResourcesInput', {
        'state': fields.String(description='Alive, absent, or dead', enum=['Alive', 'Absent', 'Dead']),
        'menmode': fields.Integer(),
        'cpufreq': fields.Integer(),
        'besteffort': fields.String(enum=['YES', 'NO'], default='NO'),
        'cpu': fields.Integer(),
        'hostname': fields.String(),
    })

    media = api.model('Media', {
        'id': fields.Integer(),
        'mode': fields.Integer(),
        'mtime': fields.Integer(),
        'name': fields.String(),
        'size': fields.Integer(),
        'type': fields.String(),
    })

    configr = api.model('Config', {
        'id': fields.Integer(),
        'links': fields.FormattedString(""),
        'value': fields.String(description='the value of the given variable'),
        'path': fields.String(description='The path of the config file'),
        'file': fields.String(description='The raw content of the config file'),
    })

    admission_rules = api.model('Admission_rules', {
        'id': fields.Integer(),
        'rule': fields.String(description='rule written in Perl applied when a job is going to be registered'),
        'uri': fields.String(),
        'links': fields.FormattedString("", description='links to previous, current and next admission rules'),
        'items': fields.String(description='list of admission rules'),
        'offset': fields.Integer(description='current offset'),
        'total': fields.Integer(description='total of admission rules'),
        'status': fields.String(description='deleted or not'),
        'method': fields.String(description='if given, the admission rule is deleted')
    })

    #####################################################################
    ########################    DAO    ##################################
    #####################################################################

    jobDAO = JobDAO()
    resourceDAO = ResourceDAO()
    arDAO = AdmissionRuleDAO()
    mediaDAO = MediaDAO()

    register_front_routes(nsFrontend, None, api)
    register_job_routes(nsJobs, jobDAO, resourceDAO, api, job, jobInit, resources, parser)
    register_media_routes(nsMedia, mediaDAO, api, media, parser)
    register_adrules_routes(nsAdmission_Rules, None, api, admission_rules)
    register_resource_routes(nsResources, nsJobs, resourceDAO, api, resources, resources_details, resources_others, resources_generate, resources_inputs, parser)
    register_colmet_routes(nsColmet, None, api)
    register_config_routes(nsConfig, None, api, configr)

    return app


#####################################################################
########################    Main    #################################
#####################################################################

if __name__ == '__main__':
    app = create_app()
    app.run(debug=True)






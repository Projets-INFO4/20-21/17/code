# -*- coding: utf-8 -*-

import json
import time
import os
import re
import ast

from sqlalchemy.orm import Query, Load
from subprocess import PIPE
from flask import Flask, g, abort, url_for, request, Response, make_response
from flask_restx import Api, Resource, fields, reqparse
from werkzeug.middleware.proxy_fix import ProxyFix
from oar.new_rest_api.utils import WSGIProxyFix, PrefixMiddleware, list_paginate

from oar import VERSION
from passlib.apache import HtpasswdFile
from oar.lib import config, db, Job, Queue, Resource as Rsrc
from oar.lib.resource_handling import remove_resource, get_count_busy_resources, set_resource_state
from oar.new_rest_api.query import APIQuery, APIQueryCollection
from oar.lib.submission import JobParameters, Submission, check_reservation
from oar.cli.oardel import oardel
from oar.cli.oarhold import oarhold
from oar.cli.oarresume import oarresume
from .job import user_and_filename_setup

import oar.lib.tools as tools


class MediaDAO(object):
    def __init__(self):
        self.counter = 0
        self.medias = []

    def get(self, id):
        for media in self.medias:
            if media['id'] == id:
                return media
        api.abort(404, "Media {} doesn't exist".format(id))

    def create(self, data):
        media = data
        media['id'] = self.counter = self.counter + 1
        self.medias.append(media)
        return media

    def update(self, id, data):
        media = self.get(id)
        media.update(data)
        return media

    def delete(self, id):
        media = self.get(id)
        self.medias.remove(media)


######################    Media    #################################


def register_media_routes(nspace, attributed_dao, api, media, parser):

    if "OARDIR" not in os.environ:
        os.environ["OARDIR"] = "/usr/local/lib/oar"
    OARDODO_CMD = os.environ["OARDIR"] + "/oardodo/oardodo"
    if "OARDODO" in config:
        OARDODO_CMD = config["OARDODO"]

    # GET /media/ls/<file_path>
    @nspace.route("/ls/<path:file_path>", doc={"description": "Get a list of the directory from the path given by file_path. The file_path may contain the special character “~” that is expanded to the home directory of the user that is making the request."})
    class MediaLs(Resource):
        @api.doc(security='basicAuth')
        @nspace.doc(responses={404: 'Path doesn\'t exist', 403: 'Path is not readable', 200: 'Success'})
        # @nspace.marshal_list_with(media, mask="mode, mtime, name, size, type")
        def get(self, file_path):
            g.current_user = request.headers.get('X_REMOTE_IDENT')
            if not g.current_user:
                return [], 403
            path, env = user_and_filename_setup(file_path)

            # Check directory's existence
            retcode = tools.call([OARDODO_CMD, "test", "-d", path], env=env)
            if retcode:
                abort(404, "Path not found: {}".format(path))

            # Check directory's readability
            retcode = tools.call([OARDODO_CMD, "test", "-r", path], env=env)
            if retcode:
                abort(403, "File could not be read: {}".format(path))

            # Check if it's a directory
            file_listing = tools.check_output([OARDODO_CMD, "ls"], env=env).decode().split("\n")
            files_with_path = [path + "/" + filename for filename in file_listing[:-1]]

            # Get the listing stat -c "%f_%s_%Y_%F_%n"
            ls_results = (tools.check_output([OARDODO_CMD, "stat", "-c", "%f_%s_%Y_%F"] + files_with_path, env=env).decode().split("\n"))
            file_stats = []
            for i, ls_res in enumerate(ls_results[:-1]):
                f_hex_mode, f_size, f_mtime, f_type = ls_res.split("_")
                file_stats.append(
                    {
                        "name": file_listing[i],
                        "mode": int(f_hex_mode, 16),
                        "size": int(f_size),
                        "mtime": int(f_mtime),
                        "type": f_type,
                    }
                )

            list_paginated = list_paginate(file_stats, 0, int(f_size))

            g.data = {}
            g.data["total"] = len(list_paginated)
            url = url_for("%s_%s" % ("media_media", "ls"), file_path=path)
            g.data["links"] = [{"rel": "rel", "href": url}]
            g.data["offset"] = 0
            g.data["items"] = list_paginated

            return g.data, 200

    @nspace.route("/<path:file_path>")
    @nspace.doc(params= {'tail': 'specifies an optional number of lines for printing only the tail of a text file'})
    @nspace.doc(responses= {404: 'File does not exist', 403: 'File is not readable', 200: 'Success'})
    class Media(Resource):

        # GET /media/<file_path>
        @api.doc(security='basicAuth')
        @nspace.doc(description='Get a file located on the API host, into the path given by file_path. The file_path may contain the special character “~” that is expanded to the home directory of the user that is making the request.')
        def get(self, file_path):
            g.current_user = request.headers.get('X_REMOTE_IDENT')
            if not g.current_user:
                return [], 403
            path_filename, env = user_and_filename_setup(file_path)

            # Check file's existence
            retcode = tools.call([OARDODO_CMD, "test", "-f", path_filename], env=env)
            if retcode:
                abort(404, "File not found: {}".format(path_filename))

            # Check file's readability
            retcode = tools.call([OARDODO_CMD, "test", "-r", path_filename], env=env)
            if retcode:
                abort(403, "File could not be read: {}".format(path_filename))

            file_content = None
            args = parser.parse_args()
            tail = args['tail']
            if tail:
                file_content = tools.check_output([OARDODO_CMD, "tail", "-n", str(tail), path_filename], env=env)
            else:
                file_content = tools.check_output([OARDODO_CMD, "cat", path_filename], env=env)
            return Response(file_content, 200, mimetype="application/octet-stream")

        # POST /media/<file_path>
        @api.doc(security='basicAuth')
        @nspace.doc(description='Upload or create a file on the API host, into the path given by file_path. The file_path may contain the special character “~” that is expanded to the home directory of the user that is making the request. If the path does not exists, the directories are automatically created. If no data is passed, an empty file is created. If binary data is sent as POSTDATA, then it is a file to upload.')
        def post(self, file_path):
            g.current_user = request.headers.get('X_REMOTE_IDENT')
            if not g.current_user:
                return [], 403
            path_filename, env = user_and_filename_setup(file_path)

            # Check file's existence
            retcode = tools.call([OARDODO_CMD, "test", "-f", path_filename], env=env)
            if not retcode:
                abort(403, "The file already exists: {}".format(path_filename))

            cmd = [OARDODO_CMD, "bash", "--noprofile", "--norc", "-c", "cat > " + path_filename]
            if request.headers["Content-Type"] == "application/octet-stream":
                p = tools.Popen(cmd, env=env, stdin=PIPE)
                try:
                    p.communicate(request.data)
                except Exception as ex:
                    p.kill()
                    abort(501, ex)
            else:
                if "file" not in request.files:
                    abort(400, "No file part")
                file = request.files["file"]
                if file.filename == "":
                    abort(400, "No selected file")
                try:
                    p = tools.Popen(cmd, env=env, stdin=file)
                except Exception as ex:
                    p.kill()
                    abort(501, ex)

            # url = url_for('%s.post_file' % app.name, path_filename=path_filename[1:])
            url = "" + path_filename

            g.data  = {}
            g.data["links"] = [{"rel": "rel", "href": url}]
            g.data["status"] = "created"
            g.data["success"] = "true"
            return g.data, 200

        # DELETE /media/<file_path>
        @api.doc(security='basicAuth')
        @nspace.doc(description='Delete the file or directory given by file_path. The file_path may contain the special character “~” that is expanded to the home directory of the user that is making the request. If the path is a directory, then it is deleted recursively.')
        def delete(self, file_path):
            g.current_user = request.headers.get('X_REMOTE_IDENT')
            if not g.current_user:
                return [], 403
            path_filename, env = user_and_filename_setup(file_path)

            # Check file's existence
            retcode = tools.call([OARDODO_CMD, "test", "-e", path_filename], env=env)
            if retcode:
                abort(404, "File not found: {}".format(path_filename))

            # Check file write permission
            retcode = tools.call([OARDODO_CMD, "test", "-w", path_filename], env=env)
            if retcode:
                abort(403, "File or directory is not writeable: {}".format(path_filename))

            # Delete the file
            retcode = tools.call([OARDODO_CMD, "rm", "-rf", path_filename], env=env)
            if retcode:
                abort(501, "File unkown error, rm -rf failed for : {}".format(path_filename))

            response = make_response("", 204)
            response.mimetype = "application/octet-stream"
            return response

    # POST /media/chmod/<file_path>
    @nspace.route("/chmod/<path:file_path>", doc={'description': 'Changes the permissions on a file: do a chmod(1) on file_path. The special character “~” is expanded as the home of the user that makes the query.'})
    class MediaChmod(Resource):
        @nspace.expect(media)
        def post(self, file_path):
            g.current_user = request.headers.get('X_REMOTE_IDENT')
            if not g.current_user:
                return [], 403
            path_filename, env = user_and_filename_setup(file_path)

            # Check file's existence
            retcode = tools.call([OARDODO_CMD, "test", "-e", path_filename], env=env)
            if retcode:
                abort(404, "File not found: {}".format(path_filename))
            args = parser.parse_args()
            mode = args['mode']

            # Security checking
            if not mode.isalnum():
                abort(400, "Bad mode value: {}".format(mode))

            # Do the chmod
            retcode = tools.call([OARDODO_CMD, "chmod", mode, path_filename], env=env)
            if retcode:
                abort(500, "Could not set mode {} on file {}".format(mode, path_filename))

            response = make_response("", 202)
            response.mimetype = "application/octet-stream"
            return response


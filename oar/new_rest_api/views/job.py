import json
import time
import os
import re
import ast

from sqlalchemy.orm import Query, Load
from subprocess import PIPE
from flask import Flask, g, abort, url_for, request, Response, make_response
from flask_restx import Api, Resource, fields, reqparse
from werkzeug.middleware.proxy_fix import ProxyFix
from oar.new_rest_api.utils import WSGIProxyFix, PrefixMiddleware, list_paginate

from oar import VERSION
from passlib.apache import HtpasswdFile
from oar.lib import config, db, Job, Queue, Resource as Rsrc
from oar.lib.resource_handling import remove_resource, get_count_busy_resources, set_resource_state
from oar.new_rest_api.query import APIQuery, APIQueryCollection
from oar.lib.submission import JobParameters, Submission, check_reservation
from oar.cli.oardel import oardel
from oar.cli.oarhold import oarhold
from oar.cli.oarresume import oarresume

import oar.lib.tools as tools


######################    Jobs    #################################


class JobDAO(object):

    def get(self, **kwargs):
        jobs = []
        query = db.queries.get_jobs_for_user(kwargs.get('user'),
                                             kwargs.get('start_time'),
                                             kwargs.get('stop_time'),
                                             kwargs.get('states'),
                                             kwargs.get('ids'),
                                             kwargs.get('array_id'),
                                             None,
                                             True)
        for item in query:
            item = self.db_to_api(item)
            jobs_attach_links(item)
            jobs.append(item)

        return jobs

    def get_as_db_model(self, **kwargs):
        jobs = []
        query = db.queries.get_jobs_for_user(kwargs.get('user'),
                                             kwargs.get('start_time'),
                                             kwargs.get('stop_time'),
                                             kwargs.get('states'),
                                             kwargs.get('ids'),
                                             kwargs.get('array_id'),
                                             None,
                                             True)
        for item in query:
            jobs.append(item)

        return jobs

    def get_all(self):
        myJobs = []
        jobs = db.query(Job).all()
        for job in jobs:
            myJobs.append(self.db_to_data(job))
        return myJobs

    def create(self, data):
        if not g.current_user:
            return [], 403

        job_parameters = self.generate_job_parameters(data)

        error = job_parameters.check_parameters()
        # print(error)

        submission = Submission(job_parameters)
        (error, job_id_lst) = submission.submit()

        jobs = []

        query = db.queries.get_jobs_for_user(None, None, None, None, job_id_lst, None, None, False)
        for item in query:
            jobs.append(self.db_to_api(item))

        for job in jobs:
            url = url_for("jobs_jobs_id", id=job['id'])
            job["links"] = [{"rel": "rel", "href": url}]

        return jobs, 201

    def update(self, id, data):
        pass

    def delete(self, id, array=None):
        if not os.environ["USER"]:
            return [], 403

        user = os.environ["USER"]

        if array:
            cmd_ret = oardel(None, None, None, None, id, None, None, None, user, False)
        else:
            cmd_ret = oardel([id], None, None, None, None, None, None, None, user, False)

        command = cmd_ret.to_str()
        status_code = cmd_ret.get_exit_value()

        return command, status_code

    def db_to_api(self, job):
        data = {
                "id": job.id,
                "array_id": job.array_id,
                "array_index": job.array_index,
                "initial_request": job.initial_request,
                "name": job.name,
                "env": job.env,
                "type": job.type,
                "info_type": job.info_type,
                "state": job.state,
                "reservation": job.reservation,
                "message": job.message,
                "scheduler_info": job.scheduler_info,
                "user": job.user,
                "project": job.project,
                "group": job.group,
                "command": job.command,
                "exit_code": job.exit_code,
                "queue_name": job.queue_name,
                "properties": job.properties,
                "launching_directory": job.launching_directory,
                "submission_time": job.submission_time,
                "start_time": job.start_time,
                "stop_time": job.stop_time,
                "file_id": job.file_id,
                "accounted": job.accounted,
                "notify": job.notify,
                "assigned_moldable_job": job.assigned_moldable_job,
                "checkpoint": job.checkpoint_signal,
                "checkpoint_signal": job.checkpoint_signal,
                "stdout_file": job.stdout_file,
                "stderr_file": job.stderr_file,
                "resubmit_job_id": job.resubmit_job_id,
                "suspended": job.suspended,
                "links": "",
        }
        return data

    def generate_job_parameters(self, data):
        job_parameters = JobParameters(
                job_type="PASSIVE",
                command=data.get('command'),
                resource=data.get('resource'),
                workdir=data.get('workdir'),
                array_params=data.get('array_params'),
                array=data.get('array'),
                queue=data.get('queue'),
                properties=data.get('properties'),
                reservation=data.get('reservation_date'),
                checkpoint=data.get('checkpoint') if data.get('checkpoint') else 0,
                signal=data.get('signal'),
                types=data.get('types'),
                directory=data.get('directory'),
                project=data.get('project'),
                initial_request=data.get('initial_request'),
                user=data.get('user') if data.get('user') else 'Anonymous',
                name=data.get('name'),
                dependencies=data.get('dependencies'),
                notify=data.get('notify'),
                resubmit=data.get('resubmit'),
                use_job_key=data.get('use_job_key') if data.get('use_job_key') else 0,
                import_job_key_from_file=data.get('import_job_key_from_file'),
                import_job_key_inline=data.get('import_job_key_inline'),
                export_job_key_to_file=data.get('export_job_key_to_file'),
                stdout=data.get('stdout'),
                stderr=data.get('stderr'),
                hold=data.get('hold'),
        )
        return job_parameters


def register_job_routes(nspace, attributed_dao, resourceDAO, api, job, jobInit, resources, parser):

    @nspace.route("/")
    class Jobs(Resource):

        # GET /jobs
        @nspace.doc(description="List jobs (by default only the jobs in queue)")
        @nspace.doc(params=
                    {
                        'state': 'comma separated list of states for filtering the jobs. Possible values: Terminated, Running, Error, Waiting, Launching, Hold,...',
                        'array': "(integer): to get the jobs belonging to an array",
                        'start_time': "(timestamp): restrict the list to the jobs that are running or not yet started before this date. Using this parameters disables the default behavior of listing only the jobs that are in queue.",
                        'stop_time': "(timestamp): restrict the list to the jobs that are running or not yet finished at this date. Using this parameters disables the default behavior of listing only the jobs that are in queue.",
                        'user': "restrict the list to the jobs owned by this username",
                        'ids': "colon separated list of ids to get a set of jobs",
                        'offset': "offset from where to get jobs",
                        'limit': "limit to get jobs"}
                    )
        # @nspace.marshal_list_with(job, mask='id, name, user')
        def get(self):
            args = parser.parse_args()
            states = args['state'][0] if args['state'] else None            
            array_id = args['array']
            start_time = args['start_time']
            stop_time = args['stop_time']
            user = args['user']
            ids = args['ids']
            offset = args['offset']
            limit = args['limit']

            jobs = attributed_dao.get(user=user, start_time=start_time, stop_time=stop_time, states=states, ids=ids, array_id=array_id)

            if offset:
                if limit:
                    jobs = jobs[offset:(offset + limit)]
                else:
                    jobs = jobs[offset:]
            else:
                if limit:
                    jobs = jobs[:limit]

            g.data = {}
            g.data["total"] = len(jobs)
            g.data["offset"] = offset
            g.data["items"] = jobs

            return g.data, 200

        '''
       @nspace.doc(params=
           {
           'resource': 'the resources description as required by oar (example: “/nodes=1/cpu=2”)',
           'command': "a command name or a script that is executed when the job starts",
           'workdir': "the path of the directory from where the job will be submited",
           'param_file': "he content of a parameters file, for the submission of an array job. For example: {“resource”:”/nodes=1, “command”:”sleep”, “param_file”:”60n90n30”}",
           'All other option accepted by the oarsub unix command': "every long option that may be passed to the oarsub command is known as a key of the input hash. If the option is a toggle (no value), you just have to set it to “1” (for example: ‘use-job-key’ => ‘1’). Some options may be arrays (for example if you want to specify several ‘types’ for a job)",
           }
       )
       '''

        # POST /jobs
        @nspace.doc(description="Creates (submit) a new job")
        @nspace.expect(jobInit)
        # @nspace.marshal_with(job)
        @api.doc(security='basicAuth')
        def post(self):
            if api.payload:
                return attributed_dao.create(api.payload), 201
            else:
                g.current_user = request.headers.get('X_REMOTE_IDENT')

                args = parser.parse_args()
                data = args['data']
                if data:
                    data = ast.literal_eval(data)
                    if data['resource']:
                        if not isinstance(data['resource'], list):
                            data['resource'] = [data['resource']]
                    return attributed_dao.create(data)
                return [], 406

    # GET /jobs/details
    @nspace.route("/details", doc={"description": "Same as /jobs, but with more details and “resources” and “nodes” links developped."})
    @nspace.doc(params=
                {'state': "comma separated list of states for filtering the jobs. Possible values: Terminated, Running, Error, Waiting, Launching, Hold,..."}
    )
    class JobsDetails(Resource):
        # @nspace.marshal_list_with(job)
        def get(self):
            args = parser.parse_args()
            states = args['state']

            jobs = attributed_dao.get(states=states)

            g.data = {}
            g.data["items"] = jobs

            return g.data, 200

    # GET /jobs/table
    @nspace.route("/table", doc={"description": "Same as /jobs but outputs the data of the SQL job table"})
    @nspace.doc(params=
                {'state': "comma separated list of states for filtering the jobs. Possible values: Terminated, Running, Error, Waiting, Launching, Hold,..."}
    )
    class JobsTable(Resource):
        # @nspace.marshal_list_with(job)
        def get(self):
            args = parser.parse_args()
            states = args['state']

            jobs = attributed_dao.get(states=states)

            g.data = {}
            g.data["items"] = jobs

            return g.data, 200

    # GET jobs/form
    @nspace.route("/form", doc={"description": "HTML form for posting (submiting) new jobs from a browser"})
    class JobsForm(Resource):
        @api.doc(security='basicAuth')
        def get(self):
            return {'': ''}

    @nspace.route("/<int:id>")
    @nspace.doc(responses={404: 'Job not found', 200: 'Success'})
    @nspace.doc(params=
                {'id': "the id of a job"}
    )
    class JobsId(Resource):

        # GET /jobs/<id>
        @nspace.doc(description="Get infos about the given job. If /details is appended, it gives more informations, such as the expanded list of resources allocated to the job.")
        #@nspace.marshal_with(job)
        @api.doc(security='basicAuth')
        def get(self, id):
            return attributed_dao.get(ids=[id])[0], 200

        # POST /jobs/<id>
        @nspace.doc(description="Updates a job. In fact, as some clients (www browsers) doesn’t support the DELETE method, this POST resource has been created mainly to workaround this and provide another way to delete a job. It also provides checkpoint, hold and resume methods, but one should preferably use the /checkpoints, /holds and /resumptions resources.")
        @api.doc(security='basicAuth')
        def post(self, id):
            return {'': ''}

        # DELETE /jobs/<id>
        @nspace.doc(description="Delete or kill a job.")
        @api.doc(security='basicAuth')
        def delete(self, id):
            g.current_user = request.headers.get('X_REMOTE_IDENT')

            args = parser.parse_args()
            array = args['isArray']

            (command, exit_status) = attributed_dao.delete(id, array)
            status_code = 200

            # Not allowed
            if exit_status == -1:
                status_code = 403
            if exit_status == -2:
                status_code = 410
            if exit_status == -3:
                status_code = 410

            g.data = {}
            g.data["id"] = id
            g.data["cmd_output"] = command
            g.data["exit_status"] = exit_status

            return g.data, status_code

    # GET /jobs/<id>/details
    @nspace.route("/<int:id>/details", doc={"description": "Get infos about the given job. If /details is appended, it gives more informations, such as the expanded list of resources allocated to the job."})
    @nspace.doc(responses={404: 'Job not found', 200: 'Success'})
    class JobsIdDetails(Resource):
        @nspace.marshal_with(resources)
        def get(self, id):
            job = attributed_dao.get_as_db_model(ids=[id])[0]

            details = []
            details.append(attributed_dao.db_to_api(job))
            details.append(resourceDAO.get_from_jobs([job]))

            return details[1], 200

    # GET /jobs/<id>/resources
    @nspace.route("/<int:id>/resources", doc={"description": "Get resources reserved or assigned to a job"})
    @nspace.doc(responses={404: 'Job not found', 200: 'Success'})
    @nspace.doc(params=
                {'id': "the id of a job"}
    )
    class JobsIdResources(Resource):
        @nspace.marshal_with(job)
        def get(self, id):
            job = attributed_dao.get_as_db_model(ids=[id])[0]

            details = []
            details.append(attributed_dao.db_to_api(job))
            details.append(resourceDAO.get_from_jobs([job]))

            return details[1], 200

    # POST /jobs/<id>/deletions/new
    @nspace.route("/<int:id>/deletions/new", doc={"description": "Deletes a job"})
    @nspace.doc(responses={404: 'Job not found', 200: 'Success'})
    @nspace.doc(params=
                {'id': "the id of a job"}
    )
    class JobsIdDeletionNew(Resource):
        @api.doc(security='basicAuth')
        def post(self, id):
            g.current_user = request.headers.get('X_REMOTE_IDENT')
            os.environ["USER"] = g.current_user if g.current_user else ""

            args = parser.parse_args()
            array = args['isArray']

            (command, exit_status) = attributed_dao.delete(id, array)
            status_code = 200

            # Not allowed
            if exit_status == -1:
                status_code = 403
            if exit_status == -2:
                status_code = 410
            if exit_status == -3:
                status_code = 410

            g.data = {}
            g.data["id"] = id
            g.data["cmd_output"] = command
            g.data["exit_status"] = exit_status

            return g.data, status_code

    # POST /jobs/<id>/checkpoints/new
    @nspace.route("/<int:id>/checkpoints/new", doc={"description": "Send the checkpoint signal to a job"})
    @nspace.doc(responses={404: 'Job not found', 200: 'Success'})
    @nspace.doc(params=
                {'id': "the id of a job"}
    )
    class JobsIdCheckpointsNew(Resource):
        @api.doc(security='basicAuth')
        def post(self, id):
            g.current_user = request.headers.get('X_REMOTE_IDENT')
            # Because a function in oardel requires it
            os.environ["USER"] = g.current_user if g.current_user else ""

            checkpointing = True

            cmd_ret = oardel(
                [id], checkpointing, None, None, None, None, None, None, g.current_user, False
            )

            g.data = {}
            g.data["id"] = id
            g.data["cmd_output"] = cmd_ret.to_str()
            g.data["exit_status"] = cmd_ret.get_exit_value()
            return g.data

    # POST /jobs/<id>/holds/new
    @nspace.route("/<int:id>/holds/new", doc={"description": "Asks to hold a waiting job"})
    @nspace.doc(responses={404: 'Job not found', 200: 'Success'})
    @nspace.doc(params=
                {'id': "the id of a job"}
    )
    class JobsIdHoldsNew(Resource):
        @api.doc(security='basicAuth')
        def post(self, id):
            g.current_user = request.headers.get('X_REMOTE_IDENT')

            running = False

            cmd_ret = oarhold([id], running, None, None, None, g.current_user, False)

            g.data = {}
            g.data["id"] = id
            g.data["cmd_output"] = cmd_ret.to_str()
            g.data["exit_status"] = cmd_ret.get_exit_value()

            return g.data, 200

    # POST /jobs/<id>/rholds/new
    @nspace.route("/<int:id>/rholds/new", doc={"description": "Asks to hold a running job"})
    @nspace.doc(responses={404: 'Job not found', 200: 'Success'})
    @nspace.doc(params=
                {'id': "the id of a job"}
    )
    class JobsIdRHoldsNew(Resource):
        @api.doc(security='basicAuth')
        def post(self, id):
            g.current_user = request.headers.get('X_REMOTE_IDENT')

            running = True

            cmd_ret = oarhold([id], running, None, None, None, g.current_user, False)

            g.data = {}
            g.data["id"] = id
            g.data["cmd_output"] = cmd_ret.to_str()
            g.data["exit_status"] = cmd_ret.get_exit_value()

            return g.data, 200

    # POST /jobs/<id>/resumptions/new
    @nspace.route("/<int:id>/resumptions/new", doc={"description": "Asks to resume a held job"})
    @nspace.doc(responses={404: 'Job not found', 200: 'Success'})
    @nspace.doc(params=
                {'id': "the id of a job"}
    )
    class JobsIdResumptionsNew(Resource):
        @api.doc(security='basicAuth')
        def post(self, id):
            g.current_user = request.headers.get('X_REMOTE_IDENT')

            cmd_ret = oarresume([id], None, None, None, g.current_user, False)

            g.data = {}
            g.data["id"] = id
            g.data["cmd_output"] = cmd_ret.to_str()
            g.data["exit_status"] = cmd_ret.get_exit_value()

            return g.data, 200

    # POST /jobs/<id>/signals/<signal>
    @nspace.route("/<int:id>/signal/<int:signal>", doc={"description": "Send a given signal to the given job"})
    @nspace.doc(responses={404: 'Job not found', 200: 'Success'})
    @nspace.doc(params=
                {'id': "the id of a job",
                 'signal': "the number of a signal (see kill -l)"}
    )
    class JobsIdSignal(Resource):
        @api.doc(security='basicAuth')
        def post(self, id, signal=None):
            g.current_user = request.headers.get('X_REMOTE_IDENT')
            os.environ["USER"] = g.current_user if g.current_user else ""

            if signal:
                checkpointing = False
            else:
                checkpointing = True

            cmd_ret = oardel(
                [id], checkpointing, signal, None, None, None, None, None, g.current_user, False
            )

            g.data = {}
            g.data["id"] = id
            g.data["cmd_output"] = cmd_ret.to_str()
            g.data["exit_status"] = cmd_ret.get_exit_value()
            return g.data


#####################################################################
########################    FROM JOBS    ############################
#####################################################################


def user_and_filename_setup(path_filename):
    # user setup
    user = g.current_user
    oar_user_env = os.environ.copy()
    oar_user_env["OARDO_BECOME_USER"] = user

    # Security escaping
    path_filename = re.sub(r"([$,`, ])", r"\\\1", path_filename)

    # $path =~ s/(\\*)(`|\$)/$1$1\\$2/g;

    # Get the path and replace "~" by the home directory
    pw_dir = tools.getpwnam(user).pw_dir
    path_filename = "/" + path_filename
    path_filename.replace("~", pw_dir)

    return path_filename, oar_user_env


def jobs_attach_links(job):
    rel_map = (
        ("show", "self", "jobs_jobs_id"),
        #("nodes", "collection", "nodes"),
        ("resources", "collection", "resources_resources"),
    )

    job["links"] = []
    for title, rel, endpoint in rel_map:
        url = url_for("%s" % endpoint, id=job["id"])
        job["links"].append({"rel": rel, "href": url, "title": title})


def attach_resources(job, jobs_resources):
    job["resources"] = []

    for resource in jobs_resources[job["id"]]:
        resource = resource.asdict(ignore_keys=("network_address",))
        resources_attach_links(resource)
        job["resources"].append(resource)


def attach_nodes(job, jobs_resources):
    job["nodes"] = []
    network_addresses = []

    for node in jobs_resources[job["id"]]:
        node = node.asdict(ignore_keys=("id",))
        if node["network_address"] not in network_addresses:
            resources_attach_links(node)
            job["nodes"].append(node)
            network_addresses.append(node["network_address"])
# -*- coding: utf-8 -*-

from flask import Flask, g, url_for
from flask_restx import Api, Resource, fields
from werkzeug.middleware.proxy_fix import ProxyFix
from oar import VERSION

API_VERSION = "2.0.0.dev0"

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, version='1.0', title='OAR OpenAPI', description='OAR software openAPI',)

# namespace
nsStressFactor = api.namespace('stress_factor', description='OAR stress factor')


@nsStressFactor.route("/")
class StressFactor(Resource):
    def get(self):
        return{}


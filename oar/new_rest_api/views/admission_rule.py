# -*- coding: utf-8 -*-
from flask import Flask, g, url_for
from flask_restx import Api, Resource, fields
from werkzeug.middleware.proxy_fix import ProxyFix
# from oar.new_rest_api.app import create_app


######################    AdmissionRule    ############################


class AdmissionRuleDAO(object):
    def __init__(self):
        self.counter = 0
        self.ars = []

    def get(self, id):
        for ar in self.ars:
            if ar['id'] == id:
                return ar
        api.abort(404, "Admission rule {} doesn't exist".format(id))

    def create(self, data):
        ar = data
        ar['id'] = self.counter = self.counter + 1
        self.ars.append(ar)
        return ar

    def update(self, id, data):
        ar = self.get(id)
        ar.update(data)
        return ar

    def delete(self, id):
        ar = self.get(id)
        self.ars.remove(ar)


def register_adrules_routes(nspace, attributed_dao, api, admission_rules):

    @nspace.route("/", doc={"description": "Get the list of admission rules"})
    class AdmissionRules(Resource):

        # GET /admission_rules
        @api.doc(security='basicAuth')
        @nspace.marshal_with(admission_rules, mask="items, links, offset, total")
        def get(self):
            return {'Admission Rules': 'Admission Rules'}

        # POST /admission_rules
        @api.doc(security='basicAuth')
        @nspace.expect(admission_rules, mask="rule")
        @nspace.doc(description='Add a new admission rule')
        @nspace.doc(response={201: 'Rule succesfully created'})
        def post(self):
            return {'Admission Rule': 'Admission Rule'}

    @nspace.route("/<int:id>")
    class AdmissionRulesId(Resource):

        # DELETE /admission_rules/<id>
        @api.doc(security='basicAuth')
        @nspace.doc(description='Delete the admission rule identified by id')
        @nspace.marshal_with(admission_rules, mask="id, status")
        def delete(self, id):
            return {}

        # GET /admission_rules/<id>
        @api.doc(security='basicAuth')
        @nspace.doc(
            params={'tail': 'specifies an optional number of lines for printing only the tail of a text file'})
        @nspace.doc(description='Get details about the admission rule identified by id')
        @nspace.marshal_with(admission_rules, mask="id, links, rule")
        def get(self, id):
            args = parser.parse_args()
            tail = args['tail']
            ars = arDAO.ars

            # TODO process tail on medias

            return ars

        # POST /admission_rules/<id>
        @api.doc(security='basicAuth')
        @nspace.doc(description='Update or delete the admission rule given by id')
        @nspace.doc(responses={201: 'Rule succesfully updated'})
        @nspace.expect(admission_rules, mask="rule, method")
        @nspace.marshal_with(admission_rules, mask="id, rule, uri")
        def post(self, id):
            return {'Admission Rule': 'Admission Rule'}

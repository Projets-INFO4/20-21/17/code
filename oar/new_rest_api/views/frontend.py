# -*- coding: utf-8 -*-
import json
import time
import os
import re
import ast

from sqlalchemy.orm import Query, Load
from subprocess import PIPE
from flask import Flask, g, abort, url_for, request, Response, make_response
from flask_restx import Api, Resource, fields, reqparse
from werkzeug.middleware.proxy_fix import ProxyFix
from oar.new_rest_api.utils import WSGIProxyFix, PrefixMiddleware, list_paginate

from oar import VERSION
from passlib.apache import HtpasswdFile
from oar.lib import config, db, Job, Queue, Resource as Rsrc
from oar.lib.resource_handling import remove_resource, get_count_busy_resources, set_resource_state
from oar.new_rest_api.query import APIQuery, APIQueryCollection
from oar.lib.submission import JobParameters, Submission, check_reservation
from oar.cli.oardel import oardel
from oar.cli.oarhold import oarhold
from oar.cli.oarresume import oarresume

API_VERSION = "2.0.1.dev0"


######################    FrontEnd    #################################


def register_front_routes(nspace, attributed_dao, api):

    # GET /index
    @nspace.route("/index", doc={"description": "Home page for the HTML browsing"})
    class OARIndex(Resource):
        def get(self):
            """ Get all main url section pages. """
            links = []
            # endpoints = ('resources', 'jobs', 'config', 'admission_rules')
            endpoints = ("resources", "jobs")
            for endpoint in endpoints:
                links.append(
                    {
                        "rel": "collection",
                        # "href": url_for("%s.index" % endpoint),
                        "title": endpoint,
                    }
                )
            return json.loads(json.dumps({"api_version": API_VERSION, "apilib_version": API_VERSION,
                                          "oar_version": VERSION, "links": links, "api_timezone": "UTC",
                                          "api_timestamp": int(time.time())
                                          }))

    # GET /version
    @nspace.route("/version", doc={
        "description": "Gives version informations about OAR and OAR API. Also gives the timezone of the API server."})
    class OARVersion(Resource):
        def get(self):
            """Give OAR and OAR API version. Also gives the timezone of the API server."""
            return json.loads(json.dumps({"api_version": API_VERSION, "apilib_version": API_VERSION,
                                          "oar_version": VERSION, "oar_lib_version": VERSION,
                                          "oar_server_version": VERSION, "api_timezone": "UTC",
                                          "api_timestamp": int(time.time())
                                          }))

    # GET /whoami
    @nspace.route("/whoami", doc={
        "description": "Gives the name of authenticated user seen by OAR API. The name for a not authenticated user is the null string."})
    class OARWhoami(Resource):
        def get(self):
            """Give the name of the authenticated user seen by OAR API.
            The name for a not authenticated user is the null string."""
            g.current_user = request.headers.get('X_REMOTE_IDENT')
            return json.loads(json.dumps(
                {"authenticated_user": g.current_user, "api_timezone": "UTC", "api_timestamp": int(time.time())}))

    # GET /timezone
    @nspace.route("/timezone", doc={
        "description": "Gives the timezone of the OAR API server. The api_timestamp given in each query is an UTC timestamp (epoch unix time). This timezone information allows you to re-construct the local time."})
    class OARTimezone(Resource):
        def get(self):
            """Gives the timezone of the OAR API server. The api_timestamp given in each query is an UTC timestamp (epoch unix time). This timezone information allows you to re-construct the local time. Time is send by defaut (see __init__.py)"""
            return json.loads(json.dumps({"api_timezone": "UTC", "api_timestamp": int(time.time())}))

    # AUTHENTICATION
    @nspace.route("/authentication/<string:basic_user>/<string:basic_password>")
    @nspace.param('basic_user')
    @nspace.param('basic_password')
    @nspace.doc(responses = {400: "Basic authentication is not provided or not validated", 
        404: "File for basic authentication is not found", 200: "Success"
    })
    class OARAuth(Resource):
        def get(self, basic_user, basic_password):   
            """allow to test is user/password math htpasswd, can be use as workaround to avoid popup open on browser, 
        
            usefull for integrated dashboard
            """

            htpasswd_filename = None
            if not (basic_user and basic_password):
                api.abort(400)

            if "HTPASSWD_FILE" in config:
                htpasswd_filename = config["HTPASSWD_FILE"]
            elif os.path.exists("/etc/oar/api-users"):
                htpasswd_filename = "/etc/oar/api-users"
            else:
                api.abort(404)
            ht = HtpasswdFile(htpasswd_filename)

            if ht.check_password(basic_user, basic_password):
                return json.loads(json.dumps({"basic authentication": "valid"}))
            api.abort(400)



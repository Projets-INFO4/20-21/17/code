# -*- coding: utf-8 -*-

from flask import Flask, g, url_for
from flask_restx import Api, Resource, fields
from werkzeug.middleware.proxy_fix import ProxyFix


######################    Colmet    #################################


def register_colmet_routes(nspace, attributed_dao, api):

    # GET /colmet/job/<id>
    @nspace.route("/job/<int:id>", doc={"description": "Extract colmet data for a given job."})
    @nspace.doc(params={'from': 'Optional timestamp to restrict the beginning of the time interval of data to get. ',
                           'to': 'Optional timestamp to restrict the end of the time interval of data to get.',
                           'metrics': 'Coma separated list of metrics to get from colmet data files.'})
    class Colmet(Resource):
        @api.doc(security='basicAuth')
        def get(self, id):
            return{'Colmet': 'Colmet'}



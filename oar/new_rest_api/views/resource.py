# -*- coding: utf-8 -*-

import json
import time
import os
import re
import ast

from sqlalchemy.orm import Query, Load
from subprocess import PIPE
from flask import Flask, g, abort, url_for, request, Response, make_response
from flask_restx import Api, Resource, fields, reqparse
from werkzeug.middleware.proxy_fix import ProxyFix
from oar.new_rest_api.utils import WSGIProxyFix, PrefixMiddleware, list_paginate

from oar import VERSION
from passlib.apache import HtpasswdFile
from oar.lib import config, db, Job, Queue, Resource as Rsrc
from oar.lib.resource_handling import remove_resource, get_count_busy_resources, set_resource_state
from oar.new_rest_api.query import APIQuery, APIQueryCollection
from oar.lib.submission import JobParameters, Submission, check_reservation
from oar.cli.oardel import oardel
from oar.cli.oarhold import oarhold
from oar.cli.oarresume import oarresume

import oar.lib.tools as tools


#####################    Resources    ###############################


class ResourceDAO(object):
    def __init__(self):
        self.resources = []

    def get_from_jobs(self, jobs):
        resources = []
        job_resources = db.queries.get_assigned_jobs_resources(jobs)

        for job in jobs:
            for resource in job_resources[job.id]:
                resources.append(resource)
        
        return resources

    def get(self, id):
        for resource in self.resources:
            if resource['id'] == id:
                return resource
        dbResources = db.query(Rsrc).all()
        for resource in dbResources:
            if resource.id == id:
                return self.create(self.db_to_data(resource))
        api.abort(404, "Resource {} doesn't exist".format(id))

    def get_all(self, network_address, detailed= True):
        if detailed:
            resources = db.query(Rsrc)
        else:
            columns = ("id", "state", "available_upto", "network_address")
            resources = db.query(Rsrc).options(Load(Rsrc).load_only(*columns))
        if network_address is not None:
            resources = resources.filter_by(network_address=network_address)
        resources.order_by(Rsrc.id.asc())
        for resource in resources:
            self.create(self.db_to_api(resource))
        return self.resources

    def create(self, data):
        resource = data
        for r in self.resources:
            if r['id'] == resource['id']:
                return r
        self.resources.append(resource)
        return resource

    def update(self, id, data):
        resource = self.get(id)
        resource.update(data)
        return resource

    def delete(self, id, user):
        resource = self.get(id)
        self.resources.remove(resource, user)
        return remove_resource(id)

    def db_to_api(self, resource):
        data = {
                'id': resource.id,
                'type': resource.type,
                'network_address': resource.network_address,
                'state': resource.state,
                'next_state': resource.next_state,
                'finaud_decision': resource.finaud_decision,
                'next_finaud_decision': resource.next_finaud_decision,
                'state_num': resource.state_num,
                'suspended_jobs': resource.suspended_jobs,
                'scheduler_priority': resource.scheduler_priority,
                'cpuset': resource.cpuset,
                'besteffort': resource.besteffort,
                'deploy': resource.deploy,
                'expiry_date': resource.expiry_date,
                'desktop_computing': resource.desktop_computing,
                'last_job_date': resource.last_job_date,
                'available_upto': resource.available_upto,
                'last_available_upto': resource.last_available_upto,
                'drain': resource.drain,
            }
        return data


def register_resource_routes(nspace, nsJobs, attributed_dao, api, resources, resources_details, resources_others, resources_generate, resources_inputs, parser):

    # missing links in response

    # GET /resources/nodes/<network_address>
    @nspace.route("/nodes/<string:network_address>")
    # GET /resources/details
    @nspace.route("/details")
    @nspace.route("/")
    class Resources(Resource):

        # GET /resources
        @nspace.doc(description='Get the list of resources and their state')
        @nsJobs.doc(params=
                    {
                        'from': "(timestamp): restrict the list to the jobs that are running or not yet started before this date. Using this parameters disables the default behavior of listing only the jobs that are in queue.",
                        'to': "(timestamp): restrict the list to the jobs that are running or not yet finished at this date. Using this parameters disables the default behavior of listing only the jobs that are in queue."
                        }
                    )
        @nspace.marshal_with(resources_others)
        def get(self, network_address = None, detailed = None):
            """Replie a comment to the post.

            :param offset: post's unique id
            :type offset: int

            :form email: author email address
            :form body: comment body
            :reqheader Accept: the response content type depends on
                              :mailheader:`Accept` header
            :status 302: and then redirects to :http:get:`/resources/(int:resource_id)`
            :status 400: when form parameters are missing
            """
            args = parser.parse_args()
            offset = args['offset']
            limit = args['limit']
            
            resources = attributed_dao.get_all(network_address, detailed)
            processed_resources = resources.copy()
            '''
            for resource in resources:
                if network_adress is not None:
                    if resource['network_adress'] != network_adress:
                        processed_resources.remove(resource)
            '''
            if limit is not None:  
                if offset + limit <= len(resources) :
                    processed_resources = processed_resources[offset: offset + limit]
                else :
                    processed_resources = processed_resources[offset : len(resources)]
            total = len(processed_resources)

            return {"items" : processed_resources, "offset":offset, "total": total}, 200

        # POST /resources
        @api.doc(security='basicAuth')
        @nspace.doc(description='Creates a new resource or a set of new resources')
        @nspace.expect(resources_inputs)
        @nspace.marshal_with(resources, mask="id, status, uri")
        def post(self):
            args = parser.parse_args()
            properties = args['properties']
            if properties:
                properties = ast.literal_eval(properties)
            hostname = args['hostname']
            if api.payload:
                return attributed_dao.create(api.payload), 201
            else:
                g.current_user = request.headers.get('X_REMOTE_IDENT')
                props = properties
                user = g.current_user
                if (user == "oar") or (user == "root"):
                    resource_fields = {"network_address": hostname}
                    resource_fields.update(props)
                    ins = Rsrc.__table__.insert().values(**resource_fields)
                    result = db.session.execute(ins)
                    resource_id = result.inserted_primary_key[0]
                    r_id = resource_id
                    uri = url_for("%s.%s" % ("", "resources_resources"), resource_id=resource_id)
                    status = "ok"
                else:
                    
                    status = "Bad user"

            return {"id": r_id, "uri":uri, "status":status}

    '''
    def attach_job(job):
        rel_map = (
            ("show", "self", "show"),
            ("nodes", "collection", "nodes"),
            ("resources", "collection", "resources"),
        )
        job["links"] = []
        for title, rel, endpoint in rel_map:
            url = url_for("%s.%s" % ("jobs", endpoint), job_id=job["id"])
            job["links"].append({"rel": rel, "href": url, "title": title})

    @nspace.route("/<int:id>/jobs")
    @nspace.doc(description='Get the list of resources and all the details about them')
    class ResourcesDetails(Resource):
        @nspace.marshal_with(resources_others)
        def get(self, id):
            args = parser.parse_args()
            properties = args['limit']
            properties = args['offset']
            resources = attributed_dao.get(id)
            processed_resources = resources.copy()
            if limit is not None:  
                if offset + limit <= len(resources) :
                    processed_resources = processed_resources[offset: offset + limit]
                else :
                    processed_resources = processed_resources[offset : len(resources)]
            for job in processed_resources:
                attach_job(job)
            total = len(processed_resources)

            return{'items': processed_resources}
    '''

    @nspace.route("/<int:resource_id>")
    @nspace.doc(description='Get details about the resource identified by id')
    class ResourcesId(Resource):

        # GET /resources/<id>
        @nspace.marshal_with(resources)
        def get(self, resource_id):
            return attributed_dao.get(id)

        # DELETE /resources/<id>
        # @api.doc(security='basicAuth')
        @nspace.marshal_with(resources, mask="status")
        @nspace.doc(responses={403: 'The resource could not be deleted', 201: 'Successful deletion'})
        def delete(self, resource_id):
            if id:
                error, error_msg = remove_resource(resource_id, request.headers.get('X_REMOTE_IDENT'))
                if error == 0:
                    status = "Deleted"
                else:
                    status = error_msg
                
                exit_value = error
            else:
                status = "Can not determine resource id"
                exit_value = 1

            return {"id":id, "status":status, "exit_value":exit_value}, 200
            
    # POST /resources/<id>/state
    @nspace.route("/<int:resource_id>/state")
    @nspace.doc(description='Change the state')
    class ResourcesIdState(Resource):
        @api.doc(security='basicAuth')
        @nspace.expect(resources_inputs)
        @nspace.marshal_with(resources, mask="id, status, uri")
        def post(self, resource_id):

            args = parser.parse_args()
            state = args['state']
            user = request.headers.get('X_REMOTE_IDENT')

            if (user == "oar") or (user == "root"):

                set_resource_state(resource_id, state[0], "NO")

                tools.notify_almighty("ChState")
                tools.notify_almighty("Term")
                g.data = {}
                g.data["id"] = resource_id
                g.data["uri"] = url_for("%s.%s" % ("", "resources_resources"), resource_id=resource_id)
                g.data["status"] = "Change state request registered"
            else:
                g.data = {}
                g.data["status"] = "Bad user"

    # GET /resources/nodes/<network_address>
    @nspace.route("/nodes/<string:network_address>")
    @nspace.doc(description='Get details about the resources belonging to the node identified by network_address')
    class ResourcesNodes(Resource):
        @nspace.marshal_list_with(resources, mask="jobs_uri, network_address, node_uri, resource_id, state, uri")
        def get(self, network_address):
            return{'Admission Rule': 'Admission Rule'}

    # DELETE /resources/<node>/<cpuset_id>
    @nspace.route("/<int:node>/<int:cpuset_id>")
    @nspace.doc(description='Delete the resource corresponding to cpuset_id on node node. It is useful when you don’t know about the ids, but only the number of cpus on physical nodes.')
    class ResourcesNodesCpuset(Resource):
        @api.doc(security='basicAuth')
        @nspace.marshal_with(resources, mask="status")
        @nspace.doc(responses={403: 'The resource could not be deleted', 201: 'Successful deletion'})
        def delete(self, node, cpuset_id):
            return{}

    @nspace.route("/busy")
    class ResourcesBusy(Resource):
        def get(self):
            res = get_count_busy_resources()
            return {"busy": res}

    # POST /resources/generate
    @nspace.route("/generate")
    @nspace.doc(description='Generates (outputs) a set of resources using oaradmin. The result may then be directly sent to /resources for actual creation.')
    class ResourcesGenerate(Resource):
        @api.doc(security='basicAuth')
        @nspace.expect(resources_inputs)
        @nspace.marshal_with(resources_generate)
        def post(self):
            return{'Resource generated': 'Resource generated'}

# -*- coding: utf-8 -*-

from flask import Flask, g, url_for
from flask_restx import Api, Resource, fields
from werkzeug.middleware.proxy_fix import ProxyFix
from oar.new_rest_api.app import create_app


######################    Config    #################################


def register_config_routes(nspace, attributed_dao, api, configr):

    # GET /config
    @nspace.route("/", doc={"description": "Get the list of configured variables"})
    class Config(Resource):
        @api.doc(security='basicAuth')
        # @nspace.marshal_list_with(configr, mask="id, links, value")
        def get(self):
            return{'Config': 'Config'}

    # GET /config/file
    @nspace.route("/file")
    @nspace.doc(description='Get the raw config file of OAR. It also output the path of the file used by the API.')
    class ConfigFile(Resource):
        @api.doc(security='basicAuth')
        # @nspace.marshal_list_with(configr, mask="path, file")
        def get(self):
            return{'path': 'path', 'file': 'file'}

    @nspace.route("/<string:variable>")
    class ConfigVar(Resource):

        # GET /config/<variable>
        @api.doc(security='basicAuth')
        @nspace.doc(description='Changes the permissions on a file: do a chmod(1) on file_path. The special character “~” is expanded as the home of the user that makes the query.')
        # @nspace.marshal_with(configr, mask="id, links, value")
        def get(self, variable):
            return{'ConfigVar': 'ConfigVar'}

        # POST /config/<variable>
        @api.doc(security='basicAuth')
        @nspace.doc(description='Change the value of the configuration variable identified by variable')
        # @nspace.marshal_with(configr, mask="value")
        def post(self, variable):
            return{'ConfigVar': 'ConfigVar'}


# -*- coding: utf-8 -*-

import pytest
from flask import url_for
from tempfile import mkstemp
from oar.lib import config


def test_app_frontend_index(client):
    res = client.get("/index")
    print(res.json)
    print(client)
    assert res.status_code == 200 and "api_timestamp" in res.json
    assert "links" in res.json


def test_app_frontend_version(client):
    res = client.get("/version")
    print(res.json)
    assert res.status_code == 200 and "api_timestamp" in res.json
    assert "apilib_version" in res.json


def test_app_frontend_whoami(client):
    res = client.get("/whoami")
    print(res.json)
    assert res.status_code == 200 and "api_timestamp" in res.json
    assert res.json["authenticated_user"] == None


def test_app_frontend_timezone(client):
    res = client.get("/timezone")
    print(res.json)
    assert res.status_code == 200 and "api_timestamp" in res.json
    assert "api_timezone" in res.json


def test_app_frontend_authentication(client):
    _, htpasswd_filename = mkstemp()
    config["HTPASSWD_FILE"] = htpasswd_filename

    with open(htpasswd_filename, "w", encoding="utf-8") as f:
        f.write("user1:$apr1$yWaXLHPA$CeVYWXBqpPdN78e5FvbY3/")

    res = client.get(
        url_for("_oar_auth", basic_user="user1", basic_password="user1")
    )
    print(res.json)
    assert res.status_code == 200 and res.json["basic authentication"] == "valid"


def test_app_frontend_authentication_wrong_passwd(client):
    _, htpasswd_filename = mkstemp()
    config["HTPASSWD_FILE"] = htpasswd_filename

    with open(htpasswd_filename, "w", encoding="utf-8") as f:
        f.write("user1:$apr1$yWaXLHPA$CeVYWXBqpPdN78e5FvbY3/")

    res = client.get(
        url_for(
            "_oar_auth", basic_user="user1", basic_password="wrong passwd"
        )
    )
    print(res.json)
    assert res.status_code == 400

